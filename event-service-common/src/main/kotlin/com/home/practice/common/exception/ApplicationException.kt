package com.home.practice.common.exception

open class ApplicationException(
        override val message: String?,
        override val cause: Throwable? = null
) : Exception(message)
