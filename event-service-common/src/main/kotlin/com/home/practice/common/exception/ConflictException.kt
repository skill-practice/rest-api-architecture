package com.home.practice.common.exception

open class ConflictException(
        message: String?
) : ApplicationException(message)
