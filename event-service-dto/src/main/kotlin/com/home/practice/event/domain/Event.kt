package com.home.practice.event.domain

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "events")
class Event(
        @Column(name = "title")
        var title: String,

        @Column(name = "place")
        var place: String,

        @Column(name = "speaker")
        var speaker: String,

        @Column(name = "event_type")
        var eventType: String,

        @Column(name = "date_time")
        var dateTime: LocalDateTime,
) : BaseEntity()
