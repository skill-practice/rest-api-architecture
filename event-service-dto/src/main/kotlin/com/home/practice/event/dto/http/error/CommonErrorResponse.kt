package com.home.practice.event.dto.http.error

data class CommonErrorResponse(
        val message: String
)
