package com.home.practice.event.domain.request

import java.time.LocalDateTime

data class UpdateEventRequest(
        val title: String,
        val place: String,
        val speaker: String,
        val eventType: String,
        val dateTime: LocalDateTime,
)
