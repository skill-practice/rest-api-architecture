package com.home.practice.event.dto.http

import java.time.LocalDateTime

data class AddEventRequestDto(
        val title: String,
        val place: String,
        val speaker: String,
        val eventType: String,
        val dateTime: LocalDateTime,
)
