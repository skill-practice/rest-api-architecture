package com.home.practice.event.repository

import com.home.practice.event.domain.Event
import org.springframework.stereotype.Repository

@Repository
interface EventRepository : BaseRepository<Event> {
    fun findByTitle(title: String): Collection<Event>
}
