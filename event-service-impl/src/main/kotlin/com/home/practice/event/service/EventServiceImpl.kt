package com.home.practice.event.service

import com.home.practice.common.exception.NotFoundException
import com.home.practice.event.api.service.EventService
import com.home.practice.event.domain.Event
import com.home.practice.event.domain.request.AddEventRequest
import com.home.practice.event.domain.request.UpdateEventRequest
import com.home.practice.event.repository.EventRepository
import org.springframework.stereotype.Service
import java.util.UUID

@Service
internal class EventServiceImpl(
        private val repository: EventRepository
) : EventService {

    override fun createEvent(request: AddEventRequest): Event {
        val event = Event(
                title = request.title,
                place = request.place,
                speaker = request.speaker,
                eventType = request.eventType,
                dateTime = request.dateTime,
        )

        return repository.save(event)
    }

    override fun updateEvent(
            event: Event,
            request: UpdateEventRequest
    ): Event {
        return event.apply {
            title = request.title
            place = request.place
            speaker = request.speaker
            eventType = request.eventType
            dateTime = request.dateTime

            repository.save(event)
        }
    }

    override fun getEvent(id: UUID): Event {
        return repository
                .findById(id)
                .orElseThrow { NotFoundException(Event::class , id) }
    }

    override fun deleteEvent(id: UUID) {
        repository.deleteById(id)
    }

    override fun getAllEvents(): Collection<Event> {
        return repository.findAll()
    }

    override fun getAllEventsByTitle(title: String): Collection<Event> {
        return repository.findByTitle(title)
    }
}
