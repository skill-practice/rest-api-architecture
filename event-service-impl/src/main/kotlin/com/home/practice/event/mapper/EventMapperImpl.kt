package com.home.practice.event.mapper

import com.home.practice.event.api.mapper.EventMapper
import com.home.practice.event.domain.Event
import com.home.practice.event.domain.request.AddEventRequest
import com.home.practice.event.domain.request.UpdateEventRequest
import com.home.practice.event.dto.http.AddEventRequestDto
import com.home.practice.event.dto.http.EventDto
import com.home.practice.event.dto.http.UpdateEventRequestDto
import org.springframework.stereotype.Service

@Service
internal class EventMapperImpl : EventMapper {

    override fun toDto(event: Event): EventDto {
        return EventDto(
                id = event.id!!,
                title = event.title,
                place = event.place,
                speaker = event.speaker,
                eventType = event.eventType,
                dateTime = event.dateTime,
        )
    }

    override fun toAddRequest(requestDto: AddEventRequestDto): AddEventRequest {
        return AddEventRequest(
                title = requestDto.title,
                place = requestDto.place,
                speaker = requestDto.speaker,
                eventType = requestDto.eventType,
                dateTime = requestDto.dateTime,
        )
    }

    override fun toUpdateRequest(requestDto: UpdateEventRequestDto): UpdateEventRequest {
        return UpdateEventRequest(
                title = requestDto.title,
                place = requestDto.place,
                speaker = requestDto.speaker,
                eventType = requestDto.eventType,
                dateTime = requestDto.dateTime,
        )
    }
}
