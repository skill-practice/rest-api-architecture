package com.home.practice.event.useCase

import com.home.practice.event.api.mapper.EventMapper
import com.home.practice.event.api.service.EventService
import com.home.practice.event.api.useCase.EventUseCase
import com.home.practice.event.dto.http.AddEventRequestDto
import com.home.practice.event.dto.http.EventDto
import com.home.practice.event.dto.http.UpdateEventRequestDto
import org.springframework.stereotype.Service
import java.util.*

@Service
internal class EventUseCaseImpl(
        private val eventService: EventService,
        private val eventMapper: EventMapper
) : EventUseCase {

    override fun createEvent(requestDto: AddEventRequestDto): EventDto {
        val request = eventMapper.toAddRequest(requestDto)
        val event = eventService.createEvent(request)
        return eventMapper.toDto(event)
    }

    override fun updateEvent(
            id: UUID,
            requestDto: UpdateEventRequestDto
    ): EventDto {
        val event = eventService.getEvent(id)
        val request = eventMapper.toUpdateRequest(requestDto)

        eventService.updateEvent(event, request)

        return eventMapper.toDto(event)
    }

    override fun getEvent(id: UUID): EventDto {
        val event = eventService.getEvent(id)
        return eventMapper.toDto(event)
    }

    override fun deleteEvent(id: UUID) {
        eventService.deleteEvent(id)
    }

    override fun getAllEvents(): Collection<EventDto> {
        return eventService
                .getAllEvents()
                .map(eventMapper::toDto)
    }

    override fun getAllEventsByTitle(title: String): Collection<EventDto> {
        return eventService
                .getAllEventsByTitle(title)
                .map(eventMapper::toDto)
    }
}