package com.home.practice.event.api.useCase

import com.home.practice.event.dto.http.AddEventRequestDto
import com.home.practice.event.dto.http.EventDto
import com.home.practice.event.dto.http.UpdateEventRequestDto
import java.util.UUID

interface EventUseCase {
    fun createEvent(requestDto: AddEventRequestDto): EventDto
    fun updateEvent(id: UUID, requestDto: UpdateEventRequestDto): EventDto
    fun getEvent(id: UUID): EventDto
    fun deleteEvent(id: UUID)
    fun getAllEvents(): Collection<EventDto>
    fun getAllEventsByTitle(title: String): Collection<EventDto>
}
