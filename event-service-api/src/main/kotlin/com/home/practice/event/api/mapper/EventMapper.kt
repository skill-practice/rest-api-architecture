package com.home.practice.event.api.mapper

import com.home.practice.event.domain.Event
import com.home.practice.event.domain.request.AddEventRequest
import com.home.practice.event.domain.request.UpdateEventRequest
import com.home.practice.event.dto.http.AddEventRequestDto
import com.home.practice.event.dto.http.EventDto
import com.home.practice.event.dto.http.UpdateEventRequestDto

interface EventMapper {
    fun toDto(event: Event): EventDto
    fun toAddRequest(requestDto: AddEventRequestDto): AddEventRequest
    fun toUpdateRequest(requestDto: UpdateEventRequestDto): UpdateEventRequest
}
