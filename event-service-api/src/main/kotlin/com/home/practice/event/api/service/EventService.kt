package com.home.practice.event.api.service

import com.home.practice.event.domain.Event
import com.home.practice.event.domain.request.AddEventRequest
import com.home.practice.event.domain.request.UpdateEventRequest
import java.util.UUID

interface EventService {
    fun createEvent(request: AddEventRequest): Event
    fun updateEvent(event: Event, request: UpdateEventRequest): Event
    fun getEvent(id: UUID): Event
    fun deleteEvent(id: UUID)
    fun getAllEvents(): Collection<Event>
    fun getAllEventsByTitle(title: String): Collection<Event>
}
