package com.home.practice.controller

import com.home.practice.event.api.useCase.EventUseCase
import com.home.practice.event.dto.http.AddEventRequestDto
import com.home.practice.event.dto.http.EventDto
import com.home.practice.event.dto.http.UpdateEventRequestDto
import org.springframework.web.bind.annotation.*
import java.util.UUID

@RestController
@RequestMapping("/api/v1/events")
class EventControllerApi(
        private val eventUseCase: EventUseCase
) {

    @PostMapping
    fun createEvent(
            @RequestBody requestDto: AddEventRequestDto
    ): EventDto {
        return eventUseCase.createEvent(requestDto)
    }

    @PutMapping("/{id}")
    fun updateEvent(
            @PathVariable id: UUID,
            @RequestBody requestDto: UpdateEventRequestDto
    ): EventDto {
        return eventUseCase.updateEvent(id, requestDto)
    }

    @GetMapping("/{id}")
    fun getEvent(@PathVariable id: UUID): EventDto {
        return eventUseCase.getEvent(id)
    }

    @DeleteMapping("/{id}")
    fun deleteEvent(@PathVariable id: UUID) {
        eventUseCase.deleteEvent(id)
    }

    @GetMapping
    fun getAllEvents(): Collection<EventDto> {
        return eventUseCase.getAllEvents()
    }

    @GetMapping("/find-by-title")
    fun getAllEventsByTitle(title: String): Collection<EventDto> {
        return eventUseCase.getAllEventsByTitle(title)
    }
}
