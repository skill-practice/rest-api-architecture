package com.home.practice.config

import com.home.practice.event.dto.http.error.CommonErrorResponse
import com.home.practice.common.exception.ConflictException
import com.home.practice.common.exception.NotFoundException
import mu.KLogging
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.HttpStatus
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import javax.validation.ConstraintViolationException

@RestControllerAdvice
class GlobalControllerExceptionHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException::class)
    fun handleNotFoundException(exc: NotFoundException): CommonErrorResponse {
        logger.warn(exc) {}
        return CommonErrorResponse(exc.message ?: RESOURCE_NOT_FOUND)
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(ConflictException::class)
    fun handleConflictException(exc: ConflictException): CommonErrorResponse {
        logger.warn(exc) {}
        return CommonErrorResponse(exc.message ?: CONFLICT)
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun handleMethodArgumentNotValidException(exc: MethodArgumentNotValidException): CommonErrorResponse {
        logger.warn(exc) {}
        val error: FieldError? = exc.bindingResult.fieldError
        val message: String? = error?.let { "${it.field}: ${it.defaultMessage}" }

        return CommonErrorResponse(message ?: exc.message)
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException::class)
    fun handleHttpMessageNotReadableException(exc: HttpMessageNotReadableException): CommonErrorResponse {
        logger.warn(exc) {}
        return CommonErrorResponse(exc.localizedMessage
                ?: exc.message
                ?: UNPROCESSABLE)
    }


    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(ConstraintViolationException::class)
    fun handleConstraintViolationException(exc: ConstraintViolationException): CommonErrorResponse {
        logger.warn(exc) {}
        return CommonErrorResponse(exc.localizedMessage
                ?: exc.message
                ?: UNPROCESSABLE)
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(DataIntegrityViolationException::class)
    fun handleDataIntegrityViolationException(exc: DataIntegrityViolationException): CommonErrorResponse {
        logger.warn(exc) {}
        return CommonErrorResponse(exc.localizedMessage
                ?: exc.message
                ?: UNPROCESSABLE)
    }


    private companion object : KLogging() {
        private const val RESOURCE_NOT_FOUND = "Resource not found"
        private const val CONFLICT = "An error occurred, please try again later"
        private const val UNPROCESSABLE = "Unprocessable entity"
    }

}