package com.home.practice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EventApplication

fun main() {
    runApplication<EventApplication>()
}
