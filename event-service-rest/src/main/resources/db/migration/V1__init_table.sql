CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE events
(
    id        UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    title     VARCHAR(256) NOT NULL,
    place     VARCHAR(512) NOT NULL,
    speaker   VARCHAR(256) NOT NULL,
    event_type VARCHAR(64)  NOT NULL,
    date_time  TIMESTAMP
);
