INSERT INTO events(title, place, speaker, event_type, date_time)
VALUES ('event 1', 'place 1', 'speaker 1', 'type 1', now()),
       ('event 2', 'place 1', 'speaker 1', 'type 1', now()),
       ('event 3', 'place 1', 'speaker 1', 'type 1', now()),
       ('event 1', 'place 2', 'speaker 1', 'type 1', now()),
       ('event 2', 'place 2', 'speaker 2', 'type 1', now()),
       ('event 3', 'place 2', 'speaker 2', 'type 1', now()),
       ('event 1', 'place 3', 'speaker 1', 'type 1', now()),
       ('event 2', 'place 3', 'speaker 1', 'type 1', now()),
       ('event 3', 'place 4', 'speaker 1', 'type 1', now()),
       ('event 4', 'place 5', 'speaker 6', 'type 1', now());
